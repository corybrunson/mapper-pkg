set.seed(1)
x <- make_noisy_circle(n = 128, sd = .05)
sc <- mapper(x,
             filter = filter_coord(dimensions = 1),
             cover = cover_lattice(side = .7, overlap = .3),
             dye = "rainbow")
plot(sc, vertex.label = NA)
centroids <- function(x, values) {
  values <- as.matrix(values)
  t(sapply(x, function(i) apply(values[i, , drop = FALSE], 2, mean)))
}
rainbow_dyes <- dye_continuous("rainbow")(as.list(1:nrow(x)), x, centroids)
plot(x = x[, 1], y = x[, 2],
     pch = 19, cex = .5,
     col = rainbow_dyes)

x <- make_noisy_rings(n = 256, sd = .01)
biplot(prcomp(x))
sc <- mapper(x,
             filter = filter_pca(dimensions = 3),
             cover = cover_lattice(side = .3, overlap = .1),
             dye = "hsv")
plot(sc, vertex.label = NA)
