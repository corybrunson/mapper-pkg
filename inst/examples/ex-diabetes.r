library(tidyverse)
library(mapper)

# Reaven & Miller diabetes dataset
data(Diabetes, package = "heplots")
diabetes <- Diabetes %>%
  as_tibble() %>%
  select(-group)

diabetes_pca <- diabetes %>%
  as.matrix() %>%
  prcomp(scale. = FALSE) %>%
  {bind_rows(
    mutate(broom::tidy(., matrix = "u"), .matrix = "u"),
    mutate(broom::tidy(., matrix = "v"), .matrix = "v")
  )} %>%
  spread(key = PC, value = value, sep = "") %>%
  arrange(.matrix, row, column)

diabetes_pca %>%
  left_join(tibble(
    row = 1:nrow(diabetes),
    density = as.vector(filter_density(scale = FALSE)(diabetes))
  )) %>%
  dplyr::filter(.matrix == "u") %>%
  ggplot(aes(x = PC1, y = PC2)) +
  geom_point(aes(color = density)) +
  scale_color_gradientn(colors = rev(rainbow(12))[-(1:3)])

sc <- mapper(
  x = as.matrix(diabetes),
  filter = filter_density(scale = FALSE),
  cover = cover_quantiles(prob = .3),
  partition = partition_hclust(hclust_method = "single"),
  dye = dye_continuous_grdevices(palette = "rainbow")
)
plot(sc, vertex.label = NA)
