# Zachary's karate club
data(karate, package = "igraphdata")

# filter function returning graph distances from two organizers
library(igraph)
f0 <- filter_graph_distance(vids = V(karate)[c("Mr Hi", "John A")])

# Minkowski distances with different choices of power
par(mfrow = c(2, 2))
for (p in 1:4) {
  sc <- mapper(x = karate,
               filter = f0,
               cover = cover_dist(power = p, radius = 3),
               partition = partition_igraph(cluster_method = "walktrap"))
  plot(sc, vertex.label = NA)
}
# Minkowski distances with different choices of radius
for (r in 1:4) {
  sc <- mapper(x = karate,
               filter = f0,
               cover = cover_dist(power = 2, radius = r),
               partition = partition_igraph(cluster_method = "walktrap"))
  plot(sc, vertex.label = NA)
}
par(mfrow = c(1, 1))
