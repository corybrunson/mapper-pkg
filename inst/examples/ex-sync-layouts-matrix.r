# noisy circle
noisy_circle <- make_noisy_circle(n = 256)
sc <- mapper(x = noisy_circle,
             filter = filter_coord(dimensions = 1),
             cover = cover_rectangles(side = .5, overlap = .24),
             partition = "hclust")

# synchronize layouts
lays <- sync_layouts(x = noisy_circle, sc = sc, filter = filter_pca(d = 2))

# plot separately (then toggle manually)
plot(lays$x)
plot(sc, layout = lays$sc, vertex.label = NA)

cols <- color_by_vertex(noisy_circle, sc)
par(mfrow = c(1, 2))
plot(lays$x, pch = 16, col = cols$x)
plot(sc, layout = lays$sc,
     vertex.size = 10, vertex.color = cols$sc, vertex.label = NA)
par(mfrow = c(1, 1))
