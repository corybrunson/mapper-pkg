set.seed(1)
x <- make_noisy_circle(n = 128, sd = .05)
sc <- mapper(x,
             filter = filter_coord(dimensions = 1),
             cover = cover_rectangles(side = .7, overlap = .3))
cols <- color_by_vertex(x, sc)
par(mfrow = c(1, 2))
plot(x, pch = 16, col = cols$x)
plot(sc, vertex.size = 10, vertex.color = cols$sc, vertex.label = NA)
par(mfrow = c(1, 1))
