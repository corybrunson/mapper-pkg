library(tidyverse)
devtools::load_all()
x <- psych::msq %>%
  as_tibble() %>%
  select(1:75) %>%
  dist(method = "canberra")
sc <- mapper(
  x,
  filter = filter_mds(dimensions = 2),
  cover = cover_quantiles(prob = .05),
  dye = "rainbow"
)
#opts <- missing::findOptima(as.matrix(x), pareto = TRUE, to.remove = TRUE)
#opt <- opts[[3]]
# only one that doesn't require exclusions from only one dimension
opt <- list(
  structure(c(140L, 2512L, 2549L), .Names = c("140", "2512", "2549")),
  structure(c(140L, 2512L, 2549L), .Names = c("140", "2512", "2549"))
)
rm_obs <- opt %>% unlist() %>% unique()
xx <- psych::msq %>%
  as_tibble() %>%
  slice(-rm_obs) %>%
  select(1:75) %>%
  dist(method = "canberra")
sc <- mapper(
  xx,
  filter = filter_mds(dimensions = 2),
  cover = cover_quantiles(prob = .1),
  dye = "rainbow"
)

largest_component <- function(g) {
  comp <- components(g)
  induced_subgraph(g, which(comp$membership == which.max(comp$csize)))
}
plot(largest_component(sc), vertex.label = NA)
