set.seed(1)
x <- make_noisy_circle(n = 120, sd = .05)
centroids <- function(x, values) {
  values <- as.matrix(values)
  t(sapply(x, function(i) apply(values[i, , drop = FALSE], 2, mean)))
}
rainbow_dyes <- dye_continuous("rainbow")(as.list(1:nrow(x)), x, centroids)
plot(x = x[, 1], y = x[, 2],
     pch = 19, cex = .5,
     col = rainbow_dyes)
sc <- mapper(x,
             filter = filter_coord(dimensions = 1),
             cover = cover_quantiles(size = 60),
             dye = "rainbow")
plot(sc, vertex.label = NA)
sc <- mapper(x,
             filter = filter_coord(dimensions = 1),
             cover = cover_lattice(side = 1.5),
             dye = "rainbow")
plot(sc, vertex.label = NA)

set.seed(1)
x <- make_noisy_rings(n = 720, sd = .01)
sc <- mapper(x,
             filter = filter_coord(dimensions = 2),
             cover = cover_lattice(side = c(1.5, 1.5)),
             dye = "rainbow")
plot(sc, vertex.label = NA)
