# Zachary's karate club
library(igraph)
data(karate, package = "igraphdata")

# two filter functions
f0 <- filter_graph_distance(vids = V(karate)[c("Mr Hi", "John A")])
sc <- mapper(x = karate,
             filter = f0,
             cover = cover_rectangles(side = 5, overlap = 2),
             partition = "walktrap")

# synchronize layouts
lays <- sync_layouts(x = karate, sc = sc,
                     filter = filter_layout(layout_with = "fr"))

# plot separately (then toggle manually)
plot(karate, layout = lays$x)
plot(sc, layout = lays$sc, vertex.label = NA)
