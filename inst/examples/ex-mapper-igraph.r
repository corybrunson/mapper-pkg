# Zachary's karate club
library(igraph)
data(karate, package = "igraphdata")

# one-dimensional filter function
f0 <- filter_graph_distance(vids = V(karate)["John A"])
sc <- mapper(x = karate,
             filter = f0,
             cover = cover_rectangles(side = 5, overlap = 2),
             partition = "walktrap")
plot(sc,
     vertex.label.family = "sans",
     vertex.label.color = "black",
     vertex.label.cex = 2/3)
preclusters(sc, actors = "Mr Hi")
actors(sc, preclusters = c("Cluster 1", "Cluster 3"))

# two-dimensional filter function
f0 <- filter_graph_distance(vids = V(karate)["Mr Hi", "John A"])
sc <- mapper(x = karate,
             filter = f0,
             cover = cover_rectangles(side = 5, overlap = 2),
             partition = "walktrap")
plot(sc, vertex.label = NA)

# centrality-based filter function
f_cent <- function(x) cbind(
  closeness = closeness(graph = x, vids = V(x)),
  eigenvector = eigen_centrality(graph = x)$vector
)
sc <- mapper(x = karate,
             filter = f_cent,
             cover = cover_rectangles(side = .7, overlap = .6, prop = TRUE),
             partition = "walktrap")
plot(sc, vertex.label = NA)
sc <- mapper(x = karate,
             filter = f_cent,
             cover = cover_minkowski(power = 2, radius = 1, scale = sd),
             partition = "walktrap")
plot(sc, vertex.label = NA)
