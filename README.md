# an implementation of Mapper in R

This repository implements a substantial extension of [the Mapper construction of Singh et al](http://diglib.eg.org/handle/10.2312/SPBG.SPBG07.091-100) as an R package.
