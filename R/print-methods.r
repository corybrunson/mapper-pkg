#' @title Printing simplicial complexes
#'   
#' @description These functions adapt \code{\link[tibble]{format.tbl}}, 
#'   \code{\link[tibble]{print.tbl}}, and the unexported function
#'   \code{node_tibble} from \strong{tidygraph} to conveniently display data
#'   separately on the observation and cluster nodes of the bipartite
#'   \strong{igraph} object that encodes a simplicial complex.
#'   
#' @name print.mapper
#' @param x Bipartite \strong{igraph} object, representing a simplicial complex.
#' @param ... Additional parameters passed to \code{\link[tibble]{trunc_mat}}.
NULL

#' @rdname print.mapper
#' @method print sc
#' @S3method print sc
#' @export
print.sc <- function(x, ...) {
  tbl <- tibble::as_tibble(c(
    list(.vid = as.integer(V(x))),
    igraph::vertex_attr(x)
  ))
  arg_list <- list(...)
  # vertex data
  v_tbl <- tbl[tbl$type == TRUE, -match("type", names(tbl)), drop = FALSE]
  for (y in names(v_tbl)) if (!some_defined(v_tbl[[y]])) v_tbl[[y]] <- NULL
  v_trunc <- do.call(
    tibble::trunc_mat,
    utils::modifyList(arg_list, list(x = v_tbl, n = 5))
  )
  names(v_trunc$summary)[1] <- "Vertex data"
  # simplex data
  o_tbl <- tbl[tbl$type == FALSE, -match("type", names(tbl)), drop = FALSE]
  for (y in names(o_tbl)) if (!some_defined(o_tbl[[y]])) o_tbl[[y]] <- NULL
  o_trunc <- do.call(
    tibble::trunc_mat,
    utils::modifyList(arg_list, list(x = o_tbl, n = 5))
  )
  names(o_trunc$summary)[1] <- "Observation data"
  cat(
    "# A simplicial complex on ", nrow(v_tbl),
    " vertices derived from ", nrow(o_tbl),
    " observations\n",
    sep = ""
  )
  cat("#\n")
  print(o_trunc)
  cat("#\n")
  print(v_trunc)
  invisible(x)
}

some_defined <- function(x) any(is.na(x) == FALSE)
