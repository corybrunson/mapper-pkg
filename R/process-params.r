data_classes <- c("matrix", "dist", "igraph")
data_class <- function(x) {
  wh_class <- grep(paste(
    paste0("^", data_classes, "$"),
    collapse = "|"
  ), class(x))
  if (length(wh_class) == 0) return(NULL)
  class(x)[wh_class[1]]
}

universal_partitions <- c("hclust")

process_filter <- function(filter, class) {
  if (is.null(filter)) filter <- switch(
    class,
    matrix = "pca",
    dist = "mds",
    igraph = "eigencent"
  )
  if (is.character(filter)) {
    filter <- get(paste0("filter_", match.arg(
      filter,
      switch(
        class,
        matrix = c("coord", "pca", "mds"),
        dist = c("mds"),
        igraph = c("eigencent", "layout", "distance")
      )
    )))
    filter <- filter()
  }
  return(filter)
}

process_cover <- function(cover) {
  if (is.null(cover)) cover <- "rectangles"
  if (is.character(cover)) {
    cover <- get(paste0("cover_", match.arg(cover, c(
      # regular shapes
      "squares", "rectangles",
      # Minkowski balls
      "dist", "minkowski"
    ))))
    cover <- cover()
  }
  return(cover)
}

process_partition <- function(partition, class) {
  if (is.null(partition)) partition <- switch(
    class,
    matrix = "hclust",
    dist = "hclust",
    igraph = "walktrap"
  )
  if (is.character(partition)) {
    if (class %in% c("matrix", "dist")) {
      partition <- get(paste0("partition_", match.arg(partition, c(
        # numeric matrix-specific partitioning methods
        c(),
        universal_partitions
      ))))
      partition <- partition()
    } else if (class %in% c("igraph", "dist")) {
      if (partition %in% universal_partitions) {
        # universal partitioning methods (transform to 'dist' object)
        partition <- get(paste0("partition_", match.arg(partition,
                                                        universal_partitions)))
        partition <- partition()
      } else {
        # specific igraph partitioning methods
        partition <- partition_igraph(cluster_method = partition)
      }
    }
  }
  return(partition)
}

process_dye <- function(dye, dye_values) {
  if (is.null(dye)) dye <- "rainbow"
  if (is.character(dye)) {
    dye <- match.arg(tolower(dye), tolower(c(
      "continuous", "discrete",
      grdevices_palettes,
      brewer_continuous,
      brewer_discrete,
      colorspace_continuous
    )))
    if (dye %in% c("continuous", "discrete")) {
      dye <- get(paste0("dye_", match.arg(dye)))
      dye <- dye()
    } else if (dye %in% grdevices_palettes) {
      dye <- if (is.null(dye_values) | is.numeric(dye_values)) {
        dye_continuous_grdevices(dye)
      } else {
        dye_discrete_grdevices(dye)
      }
    } else if (dye %in% tolower(c(brewer_continuous, colorspace_continuous))) {
      dye <- dye_continuous(dye)
    } else if (dye %in% tolower(brewer_discrete)) {
      dye <- dye_discrete(dye)
    }
  }
  return(dye)
}
