#' @title Point cloud filter functions
#'   
#' @description These functions implement some basic filters of point cloud 
#'   data.
#'   
#' @details
#' 
#' The Mapper construction depends on a choice of \emph{filter function} 
#' \eqn{f:X\to Z}{f:X-->Z}, where \eqn{X} is (the space containing) the point
#' cloud dataset and \eqn{Z} is a topological space. The filter functions
#' implemented here have codomain \eqn{{\bf R}^n}{Reals^n}.
#' 
#' @name filter
#' @family steps in the Mapper construction
#' @param dimensions A positive integer; the dimension of the image.
#' @param dist_method Character; if \code{x} is a matrix object, passed to 
#'   \code{method} parameter of \code{\link[stats]{dist}}. If \code{x} is an 
#'   igraph object, matched to \code{"path_length_minimum"}, 
#'   \code{"resistance_distance"}, or \code{"min_cut_reciprocal"} and used to 
#'   calculate a distance matrix. If \code{NULL}, defaults to \code{"euclidean"}
#'   (matrix case) or \code{"adjacency"} (igraph case).
#' @param vids A vector of \strong{igraph} vertex IDs or names.
#' @param center,scale,diag Logical; passed to other methods \code{\link{scale}}
#'   and \code{\link{eigen_centrality}}.
#' @param layout_with Character; the suffix of a \code{\link[igraph]{layout}} 
#'   function.
#' @param ... Additional arguments passed to internal functions.
#' @return A function that takes a single matrix or data frame \code{x} and 
#'   returns a numeric matrix having \code{nrow(x)} rows and \code{dimensions} 
#'   columns, representing the evaluation in \eqn{Z} of the filter function at 
#'   each point of \eqn{X}.
NULL

#' @rdname filter
#' @export
filter_trivial <- function() identity

#' @rdname filter
#' @export
filter_coord <- function(dimensions = 1) {
  function(x) {
    if (dimensions > ncol(x)) {
      dimensions <- min(dimensions, ncol(x))
      warning("'x' has fewer than 'dimensions' dimensions.")
    }
    x[, 1:dimensions, drop = FALSE]
  }
}

#' @rdname filter
#' @export
filter_pca <- function(dimensions = 1) {
  function(x) {
    pca <- stats::prcomp(x)
    if (dimensions > ncol(pca$x)) {
      dimensions <- min(dimensions, ncol(pca$x))
      warning("PCA has fewer than 'dimensions' dimensions.")
    }
    pca$x[, 1:dimensions, drop = FALSE]
  }
}

#' @rdname filter
#' @export
filter_mds <- function(dimensions = 1, dist_method = NULL) {
  function(x) {
    d <- to_dist(x, dist_method)
    fit <- stats::cmdscale(d, k = dimensions)
    if (is.list(fit)) return(fit$points) else return(fit)
  }
}

#' @rdname filter
#' @export
filter_diffmap <- function(dimensions = 1, ...) {
  function(x) {
    fit <- destiny::DiffusionMap(x, ...)
    fit@eigenvectors[, 1:dimensions, drop = FALSE]
  }
}

#' @rdname filter
#' @export
filter_density <- function(center = TRUE, scale = TRUE, diag = TRUE) {
  function(x) {
    x <- scale(x, center = center, scale = scale)
    if (ncol(x) == 1) {
      ker <- stats::density(x, kernel = "gaussian")
      fun <- stats::approxfun(x = ker$x, y = ker$y, method = "linear")
      dens <- fun(x)
    } else {
      H <- if (diag) ks::Hpi.diag(x) else ks::Hpi(x)
      dens <- ks::kde(x = x, H = H, eval.points = x)$estimate
    }
    matrix(dens, ncol = 1)
  }
}

#' @rdname filter
#' @export
filter_tsne <- function(dimensions = 1, ...) {
  function(x) {
    fit <- Rtsne::Rtsne(x, dims = dimensions, ...)
    fit$Y
  }
}

#' @rdname filter
#' @export
filter_graph_distance <- function(vids = 1) {
  function(x) {
    distances(graph = x, v = V(x), to = V(x)[vids])
  }
}

#' @rdname filter
#' @export
filter_eigencent <- function(scale = TRUE) {
  function(x) {
    as.matrix(eigen_centrality(x, scale = scale)$vector)
  }
}

#' @rdname filter
#' @export
filter_layout <- function(layout_with = "fr", ...) {
  layout_fun <- get(paste0("layout_with_", layout_with), asNamespace("igraph"))
  function(x) {
    layout_fun(x, ...)
  }
}
