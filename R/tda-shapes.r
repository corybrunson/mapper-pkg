#' @title Make TDA shapes
#'   
#' @description Generate datasets in Euclidean space for illustrative examples 
#'   of topological data analysis.
#'   
#' @name TDA-shapes
#' @param n Number of observations.
#' @param sd Standard deviation of (independent multivariate) Gaussian noise.
#' @param r Radius (relative to unit primary radius).
NULL

#' @rdname TDA-shapes
#' @export
make_noisy_circle <- function(n, sd = .01) {
  theta <- stats::runif(n = n, min = 0, max = 2 * pi)
  cbind(x = cos(theta), y = sin(theta)) +
    MASS::mvrnorm(n = n, mu = c(0, 0), Sigma = diag(x = sd, nrow = 2))
}

#' @rdname TDA-shapes
#' @export
make_noisy_rings <- function(n, sd = .01) {
  theta <- stats::runif(n = n, min = 0, max = 4 * pi)
  theta1 <- theta[theta < 2 * pi]
  theta2 <- theta[theta >= 2 * pi]
  rbind(
    cbind(x = cos(theta1), y = sin(theta1), z = 0) +
      MASS::mvrnorm(n = length(theta1),
                    mu = c(0, 0, 0), Sigma = diag(x = sd, nrow = 3)),
    cbind(x = cos(theta2) + 1, y = 0, z = sin(theta2)) +
      MASS::mvrnorm(n = length(theta2),
                    mu = c(0, 0, 0), Sigma = diag(x = sd, nrow = 3))
  )
}

#' @rdname TDA-shapes
#' @export
make_noisy_torus <- function(n, r = .25, sd = .01) {
  phi <- stats::runif(n = n, min = 0, max = 2 * pi)
  theta <- torus_density_rejection_sample(n = n, r = r)
  cbind(
    x = (1 + r * cos(theta)) * cos(phi),
    y = (1 + r * cos(theta)) * sin(phi),
    z = r * sin(theta)
  ) +
    MASS::mvrnorm(n = n, mu = c(0, 0, 0), Sigma = diag(x = sd, nrow = 3))
}

# Diagonis, Holmes, & Shahshahani (2013)
# https://projecteuclid.org/euclid.imsc/1379942050
torus_density_rejection_sample <- function(n, r) {
  x <- c()
  while (length(x) < n) {
    theta <- stats::runif(n, 0, 2 * pi)
    dens_threshold <- stats::runif(n, 0, 1 / pi)
    dens_theta <- (1 + r * cos(theta)) / (2 * pi)
    x <- c(x, theta[dens_theta > dens_threshold])
  }
  x[1:n]
}
