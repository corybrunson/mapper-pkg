#' @title Actors and communities af a simplicial complex object
#'   
#' @description These functions return the actors and the communities of an 
#'   object of class \code{sc}.
#'   
#' @details
#' 
#' \strong{grapper} stores simplicial complexes as bipartite \strong{igraph} 
#' objects with the additional \code{sc} class. The \code{type} attribute takes 
#' the value \code{FALSE} for the actors of the original graph (which define the
#' simplices of the simplicial complex) and \code{TRUE} for communities 
#' (vertices of the simplicial complex).
#' 
#' The functions \code{actors} and \code{preclusters} (also 
#' \code{precommunities}) return the nodes of the \code{sc} object corresponding
#' to the actors and to the communities, respectively. The latter function's 
#' names reflect that these communities were determined from subgraphs induced 
#' from node sets obtained as preimages under the filter function of sets in the
#' open cover.
#' 
#' If the actors incident to a specific community or set of communities are
#' desired in an \code{actors()} call, then the communities are specified by
#' \code{preclusters}. Similarly, \code{actors} may be specified in
#' \code{preclusters()}.
#' 
#' @name actors-and-preclusters
#' @param sc Bipartite \strong{igraph} object, representing a simplicial 
#'   complex.
#' @param actors,preclusters Numeric vertex IDs or character names of nodes of 
#'   \code{sc} whose incident nodes are desired (if any). Defaults to 
#'   \code{NULL}.
#' @import igraph
NULL

#' @rdname actors-and-preclusters
#' @export
actors <- function(sc, preclusters = NULL) {
  if (is.null(preclusters)) return(V(sc)[!V(sc)$type])
  if (is.numeric(preclusters) & any(V(sc)[preclusters]$type == FALSE)) {
    preclusters <- preclusters + length(which(V(sc)$type == FALSE))
  }
  stopifnot(all(V(sc)[preclusters]$type == TRUE))
  lapply(preclusters, function(precluster) {
    V(sc)[setdiff(neighborhood(graph = sc, order = 1, nodes = precluster)[[1]],
                  V(sc)[precluster])]
  })
}

#' @rdname actors-and-preclusters
#' @export
preclusters <- function(sc, actors = NULL) {
  if (is.null(actors)) return(V(sc)[V(sc)$type])
  stopifnot(all(V(sc)[actors]$type == FALSE))
  lapply(actors, function(actor) {
    V(sc)[setdiff(neighborhood(graph = sc, order = 1, nodes = actor)[[1]],
                  V(sc)[actor])]
  })
}

#' @rdname actors-and-preclusters
#' @export
precommunities <- preclusters
