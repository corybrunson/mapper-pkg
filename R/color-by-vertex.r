#' @title Synchronize colors of a cloud of points and of its simplicial vertices
#'   
#' @description This function takes a point cloud data frame \code{x} and a 
#'   simplicial complex generated from \code{x} using \code{\link{mapper}}, and 
#'   produces a synchronized coloring scheme based on the vertices of the 
#'   complex. For coloring schemes based on filter functions, see
#'   \code{\link{dye}}.
#'   
#' @param x A dataset, formatted as a point cloud (a numeric matrix or data 
#'   frame coercible to numeric).
#' @param sc Bipartite \strong{igraph} object, representing a simplicial 
#'   complex, with \code{nrow(x)} nodes of \code{type} \code{FALSE}.
#' @example inst/examples/ex-color-by-vertex.r
#' @export
color_by_vertex <- function(x, sc) {
  stopifnot(nrow(x) == length(which(V(sc)$type == FALSE)))
  
  # assign colors to communities
  comm_cols <- grDevices::rainbow(length(which(V(sc)$type == TRUE)))
  
  # derive colors for points
  pt_cols <- sapply(neighborhood(sc, 1, 1:nrow(x)), function(y) {
    if (length(y) == 1) return(NA)
    comm_cols[y[-1] - nrow(x)] %>%
      grDevices::col2rgb() %>% `/`(255) %>% t() %>%
      grDevices::convertColor(from = "sRGB", to = "Lab") %>%
      apply(2, mean) %>%
      grDevices::convertColor(from = "Lab", to = "sRGB") %>%
      grDevices::rgb()
  })
  
  # return schemes
  list(x = pt_cols, sc = comm_cols)
}
