#' @title Mapper
#'   
#' @description This function performs the Mapper construction on datasets in
#'   several formats.
#'   
#' @param x A dataset, formatted as a point cloud (a numeric matrix or data 
#'   frame coercible to numeric), a \code{\link{dist}} object, or an 
#'   \code{\link{igraph}} object.
#' @param filter A function taking a numeric matrix \code{x} and returning a 
#'   numeric matrix having \code{nrow(x)} rows and at most \code{ncol(x)} 
#'   columns, or a character indicating which implemented filtering function to 
#'   use (see \code{\link{filter}}).
#' @param cover A function taking a numeric matrix \code{x} and returning a list
#'   of subsets of row indices of \code{x} whose union is \code{1:nrow(x)}, or a
#'   character indicating which implemented covering function to use (see 
#'   \code{\link{cover}}).
#' @param partition A function taking a numeric matrix \code{x} and returning an
#'   integer vector of length \code{nrow(x)}, or a character indicating which 
#'   implemented partitioning function to use (see \code{\link{partition}}).
#' @param dye A function taking a numeric matrix \code{img} (the filter function
#'   values) and returning a character vector of length \code{nrow(img)}, or a 
#'   character indicating which implemented dyeing function to use (see 
#'   \code{\link{dye}}). \code{NULL} produces a simplicial complex with black 
#'   vertices.
#' @param dye_values,dye_fun Optional specifications for \code{dye}.
#' @param names Character vector of length \code{nrow(x)} or \code{vcount(x)}. 
#'   If \code{NULL}, rownames or vertex names are used if available.
#' @param labels Character vector of length \code{nrow(x)} or \code{vcount(x)}. 
#'   If \code{NULL}, rownames or vertex labels (or else names) are used if
#'   available.
#' @return A simplicial complex represented as a bipartite \code{igraph} object
#'   of primary class \code{sc}.
#' @import igraph
#' @importFrom viridis viridis magma plasma inferno
#' @example inst/examples/ex-mapper-matrix.r
#' @example inst/examples/ex-mapper-igraph.r
#' @export
mapper <- function(
  x,
  filter = NULL,
  cover = NULL,
  partition = NULL,
  dye = NULL, dye_values = NULL, dye_fun = NULL,
  names = NULL, labels = NULL
) {
  if (is.null(data_class(x))) {
    stop("No method is implemented for objects of class '", class(x), "'.")
  }
  
  # process parameters
  filter <- process_filter(filter, data_class(x))
  cover <- process_cover(cover)
  partition <- process_partition(partition, data_class(x))
  dye <- process_dye(dye, dye_values)
  
  # compute image
  img <- filter(x)
  
  # compute cover
  img_cover <- cover(img)
  
  # select the appropriate subsetting function
  sub_fun <- get(paste0("sub_", data_class(x)))
  # partition each preimage
  partitions <- unname(apply(img_cover, 2, function(ids) {
    y <- sub_fun(x, ids)
    mems <- partition(y)
    lapply(unique(mems), function(p) which(ids)[mems == p])
  }))
  # list parts without nesting
  parts <- unlist(partitions, recursive = FALSE)
  
  # assign colors to parts
  dyes <- if (is.null(dye)) {
    rep("#000000", length(parts))
  } else {
    if (is.null(dye_values)) dye_values <- img
    if (is.null(dye_fun)) {
      dye_fun <- if (is.numeric(dye_values)) "centroid" else "piechart"
    }
    dye_fun <- get(dye_fun)
    # for "piechart" option, return named list:
    # $values = [list of numeric vectors, of length 'length(parts)']
    # $pie.color = [list of colors, of length 'length(unique(dye_values))']
    # otherwise return vector of colors of length 'length(parts)'
    dye(parts, values = dye_values, fun = dye_fun)
  }
  
  # prepare labels for the simplicial complex
  size_fun <- get(paste0("size_", data_class(x)))
  names_fun <- get(paste0("names_", data_class(x)))
  labels_fun <- get(paste0("labels_", data_class(x)))
  assign_names_fun <- get(paste0("assign_names_", data_class(x)))
  if (is.null(names_fun(x))) x <- assign_names_fun(x, 1:size_fun(x))
  if (is.null(names)) names <- names_fun(x)
  if (is.null(labels)) labels <- labels_fun(x)
  # build a simplicial complex from the partition intersections
  sc <- simplicialize_parts(
    parts = parts,
    names = names, labels = labels, dyes = dyes
  )
  # color attributes for original dataset based on 'dye_values'
  obs_nodes <- which(vertex_attr(sc, "type") == FALSE)
  obs_coords <- as.data.frame(img)
  for (i in seq_along(obs_coords)) {
    sc <- set_vertex_attr(sc, names(obs_coords)[i], obs_nodes, obs_coords[[i]])
  }
  obs_color <- dye(as.list(seq_along(obs_nodes)), dye_values, dye_fun)
  sc <- set_vertex_attr(sc, "color", obs_nodes, obs_color)
  
  # add simplicial complex class to igraph object
  class(sc) <- c("sc", class(sc))
  # add attributes
  attr(sc, "filter") <- filter
  attr(sc, "cover") <- cover
  attr(sc, "partition") <- partition
  attr(sc, "dye") <- dye
  
  sc
}

# partition each preimage using clustering on the data subset
sub_matrix <- function(x, ids) x[ids, , drop = FALSE]
sub_dist <- function(x, ids) stats::as.dist(as.matrix(x)[ids, ids])
sub_igraph <- function(x, ids) induced_subgraph(graph = x, vids = V(x)[ids])

# size of data object
size_matrix <- function(x) nrow(x)
size_dist <- function(x) attr(x, "Size")
size_igraph <- function(x) vcount(x)
# check for labels
names_matrix <- function(x) rownames(x)
names_dist <- function(x) attr(x, "Labels")
names_igraph <- function(x) vertex_attr(x, "name")
labels_matrix <- names_matrix
labels_dist <- names_dist
labels_igraph <- function(x) {
  if (is.null(vertex_attr(x, "label"))) {
    vertex_attr(x, "name")
  } else {
    vertex_attr(x, "label")
  }
}
# assign labels
assign_names_matrix <- function(x, labels) {
  rownames(x) <- labels
  x
}
assign_names_dist <- function(x, labels) {
  attr(x, "Labels") <- labels
  x
}
assign_names_igraph <- function(x, labels) {
  set_vertex_attr(x, "name", value = labels)
}

# build a simplicial complex from the intersections of the parts
simplicialize_parts <- function(parts, names, labels, dyes) {
  
  # construct bigraph
  bigraph_links <- do.call(rbind, lapply(seq_along(parts), function(i) {
    cbind(as.numeric(parts[[i]]), i + length(names))
  }))
  bigraph <- make_bipartite_graph(
    c(rep(0, length(names)), rep(1, length(parts))),
    t(bigraph_links),
    directed = FALSE
  )
  
  # vertex colors
  if (is.list(dyes)) {
    #V(bigraph)$pie.color <- list(dyes$pie.color)
    #V(bigraph)[V(bigraph)$type == TRUE]$pie <- dyes$values
    bigraph <- set_vertex_attr(
      bigraph, "pie.color",
      which(vertex_attr(bigraph, "type") == TRUE), list(dyes$pie.color)
    )
    bigraph <- set_vertex_attr(
      bigraph, "pie",
      which(vertex_attr(bigraph, "type") == TRUE), dyes$values
    )
    bigraph <- set_vertex_attr(
      bigraph, "shape",
      which(vertex_attr(bigraph, "type") == TRUE), "pie"
    )
  } else {
    #V(bigraph)[V(bigraph)$type == TRUE]$color <- dyes
    bigraph <- set_vertex_attr(
      bigraph, "color",
      which(vertex_attr(bigraph, "type") == TRUE), dyes
    )
  }
  
  # remove duplicate communities
  # IMPLEMENT USING 'sort' AND 'unique'?
  duped_communities <- V(bigraph)[c()]
  for (k in 1:max(degree(graph = bigraph, v = V(bigraph)[V(bigraph)$type]))) {
    k_communities <- V(bigraph)[V(bigraph)$type & degree(bigraph) == k]
    sorted_egos <- matrix(as.numeric(unlist(ego(
      graph = bigraph, order = 1,
      nodes = k_communities
    ))), nrow = k + 1)[-1, , drop = FALSE]
    if (length(sorted_egos) == 0) next
    sorted_egos <- apply(sorted_egos, 2, sort)
    duped <- duplicated(sorted_egos, MARGIN = 2)
    duped_communities <- c(duped_communities, k_communities[duped])
  }
  bigraph <- delete_vertices(graph = bigraph, v = duped_communities)
  # remove any communities with no members
  bigraph <- delete_vertices(graph = bigraph,
                             v = which(V(bigraph)$type & degree(bigraph) == 0))
  
  n_clusters <- length(which(V(bigraph)$type == TRUE))
  V(bigraph)$name <- c(names, paste("Cluster", 1:n_clusters))
  V(bigraph)$label <- c(labels, paste0("C", 1:n_clusters))
  
  bigraph
}

as.igraph.sc <- function(x, ...) {
  class(x) <- "igraph"
  x
}
