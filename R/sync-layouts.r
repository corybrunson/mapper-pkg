#' @title Synchronize visual layouts for a graph and its simplicial complex
#'   
#' @description This function takes a \strong{mapper} dataset \code{x} and a 
#'   simplicial complex generated from \code{x} using \code{\link{mapper}}, 
#'   and produces layouts for both with synchronized coordinates.
#'   
#' @param x A \strong{mapper} dataset (see \code{\link{mapper}}), or a 
#'   \code{mapper} object whose dataset \code{x$x} is a \strong{mapper} dataset.
#' @param sc A bipartite \strong{igraph} object representing the nerve complex 
#'   of \code{x}.
#' @param filter A \code{\link{filter}} function of at least two dimensions, or
#'   a character indicating which filtering function to use.
#' @example inst/examples/ex-sync-layouts-matrix.r
#' @example inst/examples/ex-sync-layouts-igraph.r
#' @export
sync_layouts <- function(x, sc, filter) {
  if ("mapper" %in% class(x)) {
    if (!missing(sc)) stop("Provide either a 'mapper' object ",
                           "or a dataset and its simplicial complex.")
    sc <- x$sc
    x <- x$x
  }
  
  x_size <- if (class(x) == "matrix") nrow(x) else vcount(x)
  stopifnot(x_size == length(which(V(sc)$type == FALSE)))
  filter <- process_filter(filter)
  
  # generate normalized graph layout
  lay <- unname(filter(x)[, 1:2])
  
  # recover communities from the simplicial complex
  communities <- lapply(ego(graph = sc, order = 1, nodes = V(sc)[V(sc)$type]),
                        function(y) as.numeric(y[-1]))
  
  # calculate centroids of communities
  centroids <- t(sapply(communities, function(y) {
    comm_lay <- lay[y, , drop = FALSE]
    apply(comm_lay, 2, mean, drop = TRUE)
  }))
  
  # return layouts
  list(x = norm_coords(lay), sc = norm_coords(centroids))
}
