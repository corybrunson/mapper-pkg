#' @title Partitioning methods
#'   
#' @description These functions implement some common point cloud partitioning 
#'   algorithms.
#'   
#' @details
#' 
#' The Mapper construction invokes a non-overlapping clustering method that 
#' partitions the preimage in \eqn{X} of each set \eqn{U_\alpha}{U[a]} in the
#' open cover of \eqn{Z}. The parts that constitute this partition then become
#' the vertices of the simplicial complex representation of \eqn{X}.
#' 

#' Currently, no methods are suitable only for matrix objects. 
#' \code{partition_igraph} is suitable only for igraph objects; it can be called
#' from \code{\link{mapper}} by setting \code{partition} either to 
#' \code{"igraph"} or to the name of any community detection method in 
#' \strong{igraph}. \code{partition_hclust} is suitable for both types
#' of datasets, which are coerced to distance matrices before partitioning.
#' 

#' @name partition
#' @family steps in the Mapper construction
#' @param dist_method Character; if \code{x} is a matrix object, passed to 
#'   \code{method} parameter of \code{\link[stats]{dist}}. If \code{x} is an 
#'   igraph object, matched to \code{"path_length_minimum"}, 
#'   \code{"resistance_distance"}, or \code{"min_cut_reciprocal"} and used to 
#'   calculate a distance matrix. If \code{NULL}, defaults to \code{"euclidean"}
#'   (matrix case) or \code{"adjacency"} (igraph case).
#' @param hclust_method Character; passed to \code{method} parameter of 
#'   \code{\link[stats]{hclust}}.
#' @param gapwidth Numeric; minimum gap between distances at which to cut the 
#'   hierarchical clustering tree.
#' @param breaks Argument passed to \code{\link[graphics]{hist}}.
#' @param cluster_method Character; the name of a community detection method 
#'   implemented in \strong{igraph}.
#' @param ... Additional arguments passed to a method.
#' @return A function that takes a matrix or igraph object \code{x} and returns
#'   a membership vector indicating which points (rows of \code{x}) or nodes
#'   (vertices of \code{x}) belong to which parts of a partition.
NULL

#' @rdname partition
#' @export
partition_trivial <- function() {
  function(x) {
    n <- switch(
      data_class(x),
      matrix = nrow(x),
      dist = attr(x, "Size"),
      igraph = vcount(x)
    )
    rep(1, n)
  }
}

#' @rdname partition
#' @export
partition_hclust <- function(
  dist_method = NULL,
  hclust_method = "single",
  gapwidth = NULL, breaks = "Sturges"
) {
  function(x) {
    d <- to_dist(x, dist_method)
    if (attr(d, "Size") == 1) return(1)
    hc <- stats::hclust(d, method = hclust_method)
    if (is.null(gapwidth)) {
      h <- graphics::hist(hc$height, breaks = breaks, plot = FALSE)
      gapwidth <- mean(diff(h$breaks))
    }
    i <- which(diff(c(hc$height, Inf)) >= gapwidth)[1]
    return(stats::cutree(hc, h = hc$height[i]))
  }
}

#' @rdname partition
#' @export
partition_igraph <- function(
  dist_method = NULL,
  cluster_method,
  ...
) {
  cluster_fun <- get(paste0("cluster_", cluster_method), asNamespace("igraph"))
  function(x) {
    x <- to_igraph(x, dist_method)
    membership(cluster_fun(x, ...))
  }
}
