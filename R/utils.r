# turn the input object into an igraph object
to_igraph <- function(x, dist_method) {
  if (methods::is(x, "igraph")) {
    if (!is.null(dist_method)) warning(
      "'x' is already an 'igraph' object, so 'dist_method' will be ignored."
    )
    return(x)
  } else if (methods::is(x, "dist")) {
    if (!is.null(dist_method)) warning(
      "'x' is already a 'dist' object, so 'dist_method' will be ignored."
    )
    return(graph_from_adjacency_matrix(x, mode = "undirected", weighted = TRUE))
  } else if (methods::is(x, "matrix")) {
    if (is.null(dist_method)) dist_method <- "euclidean"
    return(graph_from_adjacency_matrix(
      stats::dist(x, method = dist_method),
      mode = "undirected", weighted = TRUE
    ))
  }
}

# turn the input object into a distance matrix
to_dist <- function(x, dist_method) {
  if (methods::is(x, "dist")) {
    if (!is.null(dist_method)) warning(
      "'x' is already a 'dist' object, so 'dist_method' will be ignored."
    )
    return(x)
  } else if (methods::is(x, "matrix")) {
    if (is.null(dist_method)) dist_method <- "euclidean"
    return(stats::dist(x, method = dist_method))
  } else if (methods::is(x, "igraph")) {
    if (is.null(dist_method)) dist_method <- "path_length_minimum"
    graph_dist_fun <- switch(
      dist_method,
      path_length_minimum = function(graph) igraph::distances(graph),
      resistance_distance = resistance_distances,
      min_cut_reciprocal = min_cut_reciprocals
    )
    return(stats::as.dist(graph_dist_fun(x)))
  }
}

# http://mathworld.wolfram.com/ResistanceDistance.html
resistance_distances <- function(graph) {
  L <- laplacian_matrix(graph)
  Gamma <- L + 1 / vcount(graph)
  Gamma_inv <- solve(Gamma)
  Omega <- matrix(diag(Gamma_inv), nrow = vcount(graph), ncol = vcount(graph)) +
    t(matrix(diag(Gamma_inv), nrow = vcount(graph), ncol = vcount(graph))) -
    2 * Gamma_inv
  Omega
}

min_cut_reciprocals <- function(graph) {
  1 / sapply(V(graph), function(i) sapply(V(graph), function(j) {
    if (i == j) Inf else {
      igraph::min_cut(graph, source = i, target = j)
    }
  }))
}

grdevices_palettes <- c(
  "rainbow", "heat.colors", "terrain.colors", "topo.colors", "cm.colors"
)
brewer_continuous <- rownames(RColorBrewer::brewer.pal.info)[
  RColorBrewer::brewer.pal.info$category != "qual"
  ]
brewer_discrete <- rownames(RColorBrewer::brewer.pal.info)[
  RColorBrewer::brewer.pal.info$category == "qual"
  ]
colorspace_continuous <- c("luv", "lab", "hsv", "rgb")
