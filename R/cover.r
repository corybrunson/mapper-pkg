#' @title Topological covers
#'   
#' @description These functions implement some natural choices of cover for the 
#'   codomain of the filter function.
#'   
#' @details
#' 
#' A \emph{cover} of a topological space \eqn{X} is a family of open sets whose 
#' union is \eqn{X}. These covering functions select subsets for the codomain 
#' \eqn{Z} of the filter function \eqn{f} passed to \code{\link{mapper}}, based
#' on common notions of distance. Each depends on additional parameters, which
#' must be fed to \code{mapper}.
#' \enumerate{
#'   \item \code{cover_rectangles} covers \eqn{Z} with overlapping rectangles of
#'   uniform \code{side} and \code{overlap} in each dimension, given either in
#'   the coordinate units (\code{prop = FALSE}) or as proportions of the range
#'   of \eqn{f(X)\subset Z}{f(X) <= Z} (\code{prop = TRUE}).
#'   \item \code{cover_squares} is an alias for \code{cover_rectangles}.
#'   \item \code{cover_minkowski} covers \eqn{Z} with balls of common 
#'   \code{radius} under the Minkowski distance with exponent \code{power}, 
#'   centered at each point in the image. (Points exactly \code{radius} from the
#'   center are included.) \code{scale} must be either a numeric vector or a 
#'   function that produces a value from a numeric vector (or the character name
#'   of such a function); the coordinates of \eqn{Z} are scaled (divided) by
#'   these values before distances are calculated.
#'   \item \code{cover_dist} is an alias for \code{cover_minkowski}.
#' }
#' 
#' @name cover
#' @family steps in the Mapper construction
#' @param prob,size,side,overlap,prop,power,radius,scale Parameters specific
#'   to different cover functions. See \strong{Details}.
#' @import igraph
#' @example inst/examples/ex-cover-matrix.r
#' @example inst/examples/ex-cover-igraph.r
#' @return A function that takes a numeric matrix \code{x} and returns a logical
#'   matrix of \code{nrow(x)} rows and \eqn{k} columns, each column of which 
#'   indicates whether each point (row of \code{x}) or node (vertex of \code{x})
#'   belongs to each of \eqn{k} sets in a cover of \code{x}.
NULL

#' @rdname cover
#' @export
cover_quantiles <- function(prob = NULL, size = NULL, overlap = NULL) {
  function(x) {
    
    if (!is.null(size)) {
      if (!is.null(prob)) stop("Provide either 'prob' or 'size', not both.")
      if (any(size > nrow(x))) stop("'size' must be less than |x|.")
      size <- rep(size, length.out = ncol(x))
    } else {
      if (is.null(prob)) {
        message("'prob' not provided; using 'prob = 0.1'.")
        prob <- .1
      }
      if (any(prob < 0 | prob > 1)) {
        stop("'prob' is inadmissible for provided image matrix.\n",
             "See 'help(cover)'.")
      }
      prob <- rep(prob, length.out = ncol(x))
    }
    # generate intervals
    if (is.null(overlap)) {
      overlap <- if (!is.null(prob)) .5 * prob else floor(size / 2)
    } else {
      if (any(overlap >= prob)) {
        stop("'overlap' must be less than 'prob'.")
      }
    }
    overlap <- rep(overlap, length.out = ncol(x))
    
    # compute intervals from quantiles and overlap
    ints <- if (!is.null(prob)) {
      lapply(seq_along(prob), function(i) {
        len <- prob[i] - overlap[i]
        lend <- seq(0, 1, len)
        lend <- lend - (lend[length(lend)] - 1) / 2
        lend <- c(0,
                  lend[lend > 0 & lend < 1 - prob[i]],
                  1 - prob[i])
        rend <- lend + prob[i]
        cbind(unname(stats::quantile(x[, i], lend)),
              unname(stats::quantile(x[, i], rend)))
      })
    } else if (!is.null(size)) {
      lapply(seq_along(size), function(i) {
        len <- size[i] - overlap[i]
        lend <- seq(1, nrow(x), len)
        lend <- lend - floor((lend[length(lend)] - nrow(x)) / 2)
        lend <- c(1,
                  lend[lend > 0 & lend < nrow(x) - size[i] + 1],
                  nrow(x) - size[i] + 1)
        rend <- lend + size[i] - 1
        matrix(sort(x[, i])[c(lend, rend)], ncol = 2)
      })
    }
    
    if (FALSE) {
      # plot image overlaid with cover
      plot(x = x[, 1], y = x[, 2], pch = 16, cex = .5, col = "#00000077")
      for (i in 1:nrow(ints[[1]])) for (j in 1:nrow(ints[[2]])) {
        rect(
          xleft = ints[[1]][i, 1], xright = ints[[1]][i, 2],
          ybottom = ints[[2]][j, 1], ytop = ints[[2]][j, 2],
          density = NA,
          col = "#00000033"
        )
      }
    }
    
    # interval indices
    mems <- matrix(FALSE, nrow = nrow(x), ncol = 1)
    inds <- rep(1, length(ints))
    maxs <- sapply(ints, nrow)
    repeat {
      # 2 * ncol(x) matrix of current intervals' minima and maxima
      xmm <- sapply(seq_along(inds), function(i) ints[[i]][inds[i], ])
      mem <- apply(
        x >= t(matrix(xmm[1, ], nrow = ncol(x), ncol = nrow(x))) &
          x <= t(matrix(xmm[2, ], nrow = ncol(x), ncol = nrow(x))),
        1, all
      )
      if (any(mem)) mems <- cbind(mems, mem)
      inds <- increment(inds, maxs, 1, 1)
      if (is.null(inds)) break
    }
    
    mems[, -1, drop = FALSE]
  }
}

#' @rdname cover
#' @export
cover_lattice <- function(side = NULL, overlap = NULL, prop = FALSE) {
  function(x) {
    iu <- index_unique(x)
    ui <- which(!duplicated(iu))
    x <- unique(unname(x[ui, , drop = FALSE]))
    if (is.null(side)) {
      if (!is.null(overlap)) {
        side <- overlap * 2
      }
      side <- apply(apply(x, 2, range), 2, diff) / 6
    }
    if (is.null(overlap)) {
      overlap <- side / 2
    }
    stopifnot(all(overlap < side))
    if (prop) {
      stopifnot(all(0 <= side & side <= 1 & 0 <= overlap & overlap <= 1))
      side <- apply(x, 2, function(y) diff(range(y)) * side)
      overlap <- apply(x, 2, function(y) diff(range(y)) * overlap)
    }
    # accumulate intersections of image with rectangles
    mins <- apply(x, 2, min) - side + overlap
    maxs <- apply(x, 2, max)
    mems <- matrix(FALSE, nrow = nrow(x), ncol = 1)
    inds <- mins
    while (!is.null(inds)) {
      inds_mat <- t(matrix(inds, nrow = ncol(x), ncol = nrow(x)))
      mem <- apply(x >= inds_mat & x < inds_mat + side, 1, all)
      if (any(mem)) {
        if (!any(apply(sweep(mems, 1, mem, "=="), 2, all))) {
          mems <- cbind(mems, mem)
        }
      }
      inds <- increment(inds, maxs, -side + overlap, side - overlap)
    }
    unname(mems[match(iu, iu[ui]), -1, drop = FALSE])
  }
}

#' @rdname cover
#' @export
cover_rectangles <- cover_lattice

#' @rdname cover
#' @export
cover_squares <- cover_rectangles

#' @rdname cover
#' @export
cover_minkowski <- function(power = 2, radius = NULL, scale = 1) {
  function(x) {
    iu <- index_unique(x)
    ui <- which(!duplicated(iu))
    x <- unique(unname(x[ui, , drop = FALSE]))
    if (is.null(radius)) {
      radius <- mean(apply(apply(x, 1, range), 2, diff) / 6)
    }
    # scale images
    if (is.character(scale)) {
      scale <- get(scale)
    }
    if (is.function(scale)) {
      scale <- apply(x, 2, scale)
    }
    x_scaled <- sweep(x = x, MARGIN = 2, STATS = scale, FUN = "/")
    # accumulate balls of given radius about all node images
    apply(x_scaled, 1, function(y) {
      (rowSums(abs(sweep(x_scaled, 2, y, "-")) ^ power)) ^ (1 / power) <= radius
    })[match(iu, iu[ui]), , drop = FALSE]
  }
}

#' @rdname cover
#' @export
cover_dist <- cover_minkowski

# cover the codomain by overlapping rectangles
increment <- function(inds, maxs, init, incr) {
  len <- length(inds)
  inits <- rep_len(init, length.out = len)
  incrs <- rep_len(incr, length.out = len)
  if (all(inds > maxs - incrs)) return(NULL)
  i <- max(which(inds <= maxs - incrs))
  new_inds <- inds
  if (i < len) new_inds[(i + 1):len] <- inits[(i + 1):len]
  new_inds[i] <- (new_inds + incrs)[i]
  new_inds
}

# calculate a numeric index for each unique row of a matrix
index_unique <- function(x) {
  if (ncol(x) == 1) {
    return(as.numeric(factor(x[, 1, drop = TRUE])))
  } else {
    return(index_unique(cbind(as.numeric(interaction(x[, 1], x[, 2])),
                              x[, -(1:2)])))
  }
}
