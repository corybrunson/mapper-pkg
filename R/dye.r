#' @title Color-coding by filter function
#'   
#' @description These functions implement some standard coloring schemes for the
#'   points or nodes in the dataset and the vertices of the nerve complex.
#'   
#' @details
#' 
#' Data exploration in Mapper is facilitated by the coloring of vertices in the 
#' nerve complex (i.e. clusters of preimages) by averaging the values of a 
#' measure of interest, usually the filter function, on the data points or nodes
#' incident to the vertex (contained in the cluster).
#' 
#' @name dye
#' @family steps in the Mapper construction
#' @param name The name of a color palette.
#' @param palette,space Parameters passed to specific palette constructors.
#' @param ... Additional arguments passed to a method.
#' @param reverse Whether to reverse the colors in a palette (after restricting
#'   to the required number).
#' @import colorspace
NULL

#' @rdname dye
#' @export
dye_continuous <- function(name = "rainbow", ...) {
  name <- tolower(name)
  if (name %in% grdevices_palettes) {
    dye_continuous_grdevices(name, ...)
  } else if (name %in% tolower(brewer_continuous)) {
    dye_continuous_brewer(name, ...)
  } else if (name %in% tolower(colorspace_continuous)) {
    dye_continuous_colorspace(space = name)
  } else {
    stop("Unrecgonized continuous palette name '", name, "'.")
  }
}

#' @rdname dye
#' @export
dye_continuous_grdevices <- function(palette = "rainbow", ...) {
  pal_fun <- get(match.arg(tolower(palette), grdevices_palettes))
  pal <- pal_fun(n = 1024, ...)
  function(x, values, fun) {
    if (!is.numeric(values))
      stop("'dye_values' must be numeric.")
    if (is.matrix(values)) values <- values[, 1]
    y <- fun(x, values)
    pal[(y - min(values)) * 1023 / diff(range(values)) + 1]
  }
}

#' @rdname dye
#' @export
dye_continuous_brewer <- function(palette = "Spectral", ...) {
  palette <- brewer_continuous[match(tolower(palette),
                                     tolower(brewer_continuous))]
  pal_fun <- grDevices::colorRampPalette(RColorBrewer::brewer.pal(
    n = RColorBrewer::brewer.pal.info$maxcolors[match(
      palette,
      rownames(RColorBrewer::brewer.pal.info)
    )], name = palette
  ))
  pal <- pal_fun(n = 1024, ...)
  function(x, values, fun) {
    if (!is.numeric(values))
      stop("'dye_values' must be numeric.")
    if (is.matrix(values)) values <- values[, 1]
    y <- fun(x, values)
    pal[(y - min(values)) * 1023 / diff(range(values)) + 1]
  }
}

#' @rdname dye
#' @export
dye_continuous_colorspace <- function(space = "HSV") {
  space <- toupper(space)
  colorspace_fun <- get(space, asNamespace("colorspace"))
  cmins <- colorspace_mins[[space]]
  crans <- colorspace_rans[[space]]
  function(x, values, fun) {
    if (ncol(values) < 3) {
      stop("'dye_colorspace' requires 3-column numeric 'values'")
    }
    vmins <- apply(values[, 1:3, drop = FALSE], 2, min)
    vrans <- apply(values[, 1:3, drop = FALSE], 2, function(w) diff(range(w)))
    y <- fun(x, values)
    colobj <- colorspace_fun(
      (y[, 1, drop = TRUE] - vmins[1]) * crans[1] / vrans[1] + cmins[1],
      (y[, 2, drop = TRUE] - vmins[2]) * crans[2] / vrans[2] + cmins[2],
      (y[, 3, drop = TRUE] - vmins[3]) * crans[3] / vrans[3] + cmins[3]
    )
    hex(colobj)
  }
}

colorspace_rans <- list(
  LUV = c(100, 200, 200),
  LAB = c(100, 200, 200),
  HSV = c(360, 1, 1),
  RGB = c(1, 1, 1)
)

colorspace_mins <- list(
  LUV = c(0, -100, -100),
  LAB = c(0, -100, -100),
  HSV = c(0, 0, 0),
  RGB = c(0, 0, 0)
)

#' @rdname dye
#' @export
dye_discrete <- function(name = "rainbow", ..., reverse = FALSE) {
  name <- tolower(name)
  if (name %in% grdevices_palettes) {
    dye_discrete_grdevices(name, reverse)
  } else if (name %in% tolower(brewer_discrete)) {
    dye_discrete_brewer(name, ..., reverse = FALSE)
  } else {
    stop("Unrecgonized discrete palette name '", name, "'.")
  }
}

#' @rdname dye
#' @export
dye_discrete_grdevices <- function(palette = "rainbow", reverse = FALSE) {
  pal_fun <- get(match.arg(palette, grdevices_palettes))
  function(x, values, fun) {
    pal <- pal_fun(n = length(unique(values)))
    if (reverse) pal <- rev(pal)
    if (! mode(values) %in% c("logical", "numeric", "character"))
      stop("'dye_values' must be a vector.")
    res <- fun(x, values)
    if (is.list(res)) {
      return(list(
        values = res,
        pie.color = pal
      ))
    } else {
      return(pal[res])
    }
  }
}

#' @rdname dye
#' @export
dye_discrete_brewer <- function(palette = "Set1", ..., reverse = FALSE) {
  palette <- brewer_discrete[match(tolower(palette), tolower(brewer_discrete))]
  brewer_name <- palette
  function(x, values, fun) {
    len <- length(unique(values))
    pal <- RColorBrewer::brewer.pal(len, brewer_name, ...)
    pal <- pal[seq_len(len)]
    if (reverse) pal <- rev(pal)
    if (! mode(values) %in% c("logical", "numeric", "character"))
      stop("'dye_values' must be a vector.")
    res <- fun(x, values)
    if (is.list(res)) {
      return(list(
        values = res,
        pie.color = pal
      ))
    } else {
      return(pal[res])
    }
  }
}

# functions to aggregate observations in order to dye a cluster
centroid <- function(x, values) {
  values <- as.matrix(values)
  t(sapply(x, function(i) apply(values[i, , drop = FALSE], 2, mean)))
}
plurality <- function(x, values) {
  values <- as.integer(as.factor(values))
  sapply(x, function(i) {
    v <- values[i]
    v[grep(names(which.max(table(v))), as.character(v))[1]]
  })
}
piechart <- function(x, values) {
  values <- as.factor(values)
  lapply(x, function(i) table(values[i]))
}
